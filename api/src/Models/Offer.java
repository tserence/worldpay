package Models;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class Offer {

  private long offerId;
  private String merchantId; //Merchant ID is unique, can be name or identifier
  private String description;
  private int price; //price is standardised by the REST api and the company providing endpoints

  //Used when inserting offer into storage as the offerId is assigned at that point
  public Offer(final String merchantId, final String description, final int price) {
    this.merchantId = merchantId;
    this.description = description;
    this.price = price;
  }

  //Used when fetching from CSV
  public Offer(final long offerId, final String merchantId, final String description, final int price) {
    this.offerId = offerId;
    this.merchantId = merchantId;
    this.description = description;
    this.price = price;
  }

  //Stored as CSV (id, merchantid, description, price)
  public String toCSV(final long id) {
    List<String> args = new ArrayList<>();
    args.add(Long.toString(id));
    args.add(merchantId);
    args.add(description);
    args.add(Integer.toString(price));

    return args.stream().collect(joining(",")) + System.getProperty("line.separator");
  }

}
