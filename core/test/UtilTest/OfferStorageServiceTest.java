package UtilTest;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import Models.Offer;
import Util.OfferStorageService;

public class OfferStorageServiceTest {

  private OfferStorageService offerStorageService;
  private Offer offer;
  private final String existingIdString = "1";
  private final long existingIdLong = 1L;

  @Before
  public void setup() {
    this.offerStorageService = new OfferStorageService();
  }

  @Test
  public void testStoreOffer_correctOfferObject_idReturned() {
    offer = new Offer("testMerchant", "test", 100);
    final String id = offerStorageService.storeOffer(offer);

    assertNotNull(id);
  }

  @Test
  public void testGetOffer_idIsStringAndExists_optionalHoldsOffer() {
    Optional<Offer> offer = offerStorageService.getOffer(existingIdString);
    assertTrue(offer.isPresent());
  }

  @Test
  public void testGetOffer_idIsLongAndExists_optionalHoldsOffer() {
    Optional<Offer> offer = offerStorageService.getOffer(existingIdLong);
    assertTrue(offer.isPresent());
  }

  @Test
  public void testGetOffer_idIsStringAndDoesNotExist_optionalIsEmpty() {
    Optional<Offer> offer = offerStorageService.getOffer("10000");
    assertFalse(offer.isPresent());
  }

  @Test
  public void testGetOffer_idIsLongAndDoesNotExist_optionalIsEmpty() {
    Optional<Offer> offer = offerStorageService.getOffer(10000L);
    assertFalse(offer.isPresent());
  }
}
