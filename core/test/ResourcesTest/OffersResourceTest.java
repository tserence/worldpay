package ResourcesTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.WebApplicationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import Models.Offer;
import Resources.OffersResource;
import Util.OfferStorageService;

@RunWith(MockitoJUnitRunner.class)
public class OffersResourceTest {

  @Mock
  private OfferStorageService offerStorageService;

  private OffersResource offersResource;
  private Offer offer;

  @Before
  public void setup() {
   offersResource = new OffersResource(offerStorageService);
  }

  @Test(expected = WebApplicationException.class)
  public void testCreateOffer_offerIsNull_throwsWorldPayException() {
    offersResource.createOffer(null);
  }

  @Test
  public void testCreateOffer_offerIsGood_offerPersistedToFile() {
    when(offerStorageService.storeOffer(any())).thenReturn("1");
    offer = new Offer("testMerchant", "A test description", 100);
    final long offerId = Long.parseLong(offersResource.createOffer(offer));

    assertNotEquals(-1, offerId);
  }

  @Test
  public void testToString_stringReturnedIsCSV() {
    offer = new Offer("testCSV", "testCSV", 100);
    assertEquals("1,testCSV,testCSV,100\n", offer.toCSV(1));
  }
}
