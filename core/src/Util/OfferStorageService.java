package Util;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

import Models.Offer;
import Models.WorldPayException;

public class OfferStorageService {

  public static String OFFER_STORAGE_FILE = "storage/offers.txt";
  public static String ID_FILE = "storage/id.txt";

  public String storeOffer(final Offer offer) {
    final Path offersFile = Paths.get(OFFER_STORAGE_FILE).toAbsolutePath();
    final Path idFile = Paths.get(ID_FILE).toAbsolutePath();

    try (FileWriter fw = new FileWriter(offersFile.toString(), true);
         BufferedReader idbr = Files.newBufferedReader(idFile)) {

      //Set new offer id
      final long nextOfferId = Long.parseLong(idbr.readLine()) + 1L;

      final FileWriter idbw = new FileWriter(idFile.toString(), false);
      final String id = Long.toString(nextOfferId);
      idbw.write(id);
      idbw.close();

      //write to offer storage
      fw.write(offer.toCSV(nextOfferId));
      return id;
    } catch (IOException e) {
      System.out.println("Log: Could not write offer to storage. " + e);
      throw new WorldPayException("Error in saving offer to the server! " + e, INTERNAL_SERVER_ERROR);
    }
  }

  public Optional<Offer> getOffer(final String id) {
    final Path offersFile = Paths.get(OFFER_STORAGE_FILE).toAbsolutePath();
    try (BufferedReader br = Files.newBufferedReader(offersFile)){

      String line;
      while ((line = br.readLine()) != null) {
        if (id.equals(line.split(",")[0])) {
          return Optional.of(csvToOffer(line));
        }
      }
      return Optional.empty();
    } catch (IOException e) {
      System.out.println("Log: Could not open storage to read offers. " + e);
    }
    return Optional.empty();
  }

  public Optional<Offer> getOffer(final long id) {
    return getOffer(Long.toString(id));
  }

  public void deleteOffer(final String id) {
    //Delete or throw exception
  }

  //Stored as CSV (id, merchantid, description, price)
  private Offer csvToOffer(final String representation) {
    String[] args = representation.split(",");

    if(representation == null || args.length != 4) {
      throw new WorldPayException("Error in reading offers", INTERNAL_SERVER_ERROR);
    }

    return new Offer(Long.parseLong(args[0]), args[1], args[2], Integer.parseInt(args[3]));
  }
}
