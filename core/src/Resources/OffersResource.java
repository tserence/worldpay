package Resources;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

import Models.Offer;
import Models.WorldPayException;
import Util.OfferStorageService;

public class OffersResource implements Offers {

  private OfferStorageService offerStorageService;

  public OffersResource(final OfferStorageService offerStorageService) {

    this.offerStorageService = offerStorageService;
  }

  @Override
  public String createOffer(final Offer offer) {
    if (offer == null) {
      throw new WorldPayException("Offer object was not specified in request", BAD_REQUEST);
    }

    return offerStorageService.storeOffer(offer);
  }


}
