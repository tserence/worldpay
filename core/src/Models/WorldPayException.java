package Models;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class WorldPayException extends WebApplicationException {

  public WorldPayException(final String message, final Response.Status status) {
    super(message, status);
  }
}
